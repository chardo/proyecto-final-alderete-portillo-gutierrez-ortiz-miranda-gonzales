package proyectofinal;

import controladores.ControladorLogin;
import java.awt.EventQueue;
import modelos.ModeloUsuario;
import vistas.VistaLogin;

public class ProyectoFinal {

    //metodo main
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ControladorLogin(new VistaLogin(), new ModeloUsuario()).iniciar();
            }
        });
    }
}
