
package controladores;

import Singleton.Conexion;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import modelos.DAOUsuario;
import modelos.ModeloUsuario;
import vistas.VistaAgregar;
import vistas.VistaLogin;
import vistas.VistaUsuario;

public class ControladorLogin implements ActionListener{
    private VistaLogin vista;
    private ModeloUsuario modelo;
    private DAOUsuario dao;
    private ResultSet rs;
    
    //Constructor
    public ControladorLogin(VistaLogin vista, ModeloUsuario modelo) {
        this.vista = vista;
        this.modelo = modelo;
        setControl();
    }
    
    //Inicializa la vista para Login
    public void iniciar(){
        vista.setTitle("Login");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }

    
    //Listener de acción de la vista
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(vista.getBtnAgregar())){//Si se presiona boton de agregar usuario
            vista.setVisible(false);
            EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ControladorAgregar(modelo, new VistaAgregar()).iniciar();
            }
        });
        }else{//si se presiona boton de login
            try {
                dao = new DAOUsuario(Conexion.crearConexion());
                rs = dao.consultar("Select * from Cliente where pass=MD5('" + vista.getTxtPass().getText() + "')");
                while(rs.next()){
                    if(rs.getString(2).equals(vista.getTxtUsusario().getText())){
                        modelo.setId(rs.getInt(1));
                        modelo.setUsuario(rs.getString(2));
                        modelo.setContrasena(rs.getString(3));
                        modelo.setSaldo(rs.getFloat(4));
                        Pasa();
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }

    //Asigna el control de los botones
    private void setControl() {
        vista.getBtnLogin().addActionListener(this);
        vista.getBtnAgregar().addActionListener(this);
    }

    //Inicia sesion si usuario y contraseña son correctos
    private void Pasa() {
        vista.setVisible(false);
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ControladorUsuario(new VistaUsuario(), modelo).iniciar();
            }
        });
    }
}
