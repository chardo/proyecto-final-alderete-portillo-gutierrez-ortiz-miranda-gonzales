
package controladores;

import Singleton.Conexion;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import modelos.DAOPeliculas;
import modelos.DAOUsuario;
import modelos.ModeloUsuario;
import vistas.VistaLogin;
import vistas.VistaPeliculas;
import vistas.VistaSaldo;
import vistas.VistaUsuario;


public class ControladorPeliculas implements ActionListener {
    
    private ModeloUsuario modelo;
    private VistaPeliculas vista;
    private DefaultTableModel tablaPeliculas;
    private DAOPeliculas dao;

    //Constructor
    public ControladorPeliculas(ModeloUsuario modelo, VistaPeliculas vista) {
        this.modelo = modelo;
        this.vista = vista;
        try{
            dao = new DAOPeliculas(Conexion.crearConexion());
        }catch(Exception e){
            Logger.getLogger(ControladorUsuario.class.getName()).log(Level.SEVERE, null, e);
        }
        setControl();
    }
    //inicializa vista de las peliculas a rentar
    public void iniciar(){
        vista.setTitle("Rentar");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        llenarTablaPelis();
    }
    
    //Asigna el control de los botones de la vista
    private void setControl(){
        vista.getBtnRenta().addActionListener(this);
        vista.getBtnSaldo().addActionListener(this);
        vista.getBtnSesion().addActionListener(this);
        vista.getBtnVer().addActionListener(this);
        vista.getTxtNombre().setText(modelo.getUsuario());
        vista.getTxtSaldo().setText("Saldo:" + modelo.getSaldo());
    }

    //listener de las acciones de la vista
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(vista.getBtnVer())){
            abrirVer();
        }else if(e.getSource().equals(vista.getBtnRenta())){
            rentar();
        }else if(e.getSource().equals(vista.getBtnSaldo())){
            abrirSaldo();
        }else if(e.getSource().equals(vista.getBtnSesion())){
            cerrarSesion();
        }
                
                
    }
    //Inserta las peliculas de la base de datos en la tabla
    private void llenarTablaPelis(){
        tablaPeliculas = buildTableModel(dao.consultar("Select nombre, precio, categoria from Peliculas"));
        vista.getTablaPelis().setModel(tablaPeliculas);
    }
    public static DefaultTableModel buildTableModel(ResultSet rs) {
        try{
            ResultSetMetaData metaData = rs.getMetaData();
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }

            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
            while (rs.next()) {
                Vector<Object> vector = new Vector<Object>();
                for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                    vector.add(rs.getObject(columnIndex));
                }
                data.add(vector);
            }
            return new DefaultTableModel(data, columnNames);
        }catch(SQLException e){
            Logger.getLogger(ControladorUsuario.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    //Abre vista del usuario
    private void abrirVer() {
        vista.setVisible(false);
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ControladorUsuario(new VistaUsuario(), modelo).iniciar();
                
            }
        });
    }
    
    //Agrega una renta al usuario
    private void rentar() {
        int selectedRow = vista.getTablaPelis().getSelectedRow();
        if(selectedRow >= 0){
            dao.rentar(modelo.getId(), tablaPeliculas.getValueAt(selectedRow, 0).toString());
            float costo = Float.parseFloat(tablaPeliculas.getValueAt(selectedRow, 1).toString());
            modelo.setSaldo(modelo.getSaldo() - costo);
            try {
                DAOUsuario daoUsu = new DAOUsuario(Conexion.crearConexion());
                daoUsu.actualizarSaldo(modelo);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ControladorPeliculas.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ControladorPeliculas.class.getName()).log(Level.SEVERE, null, ex);
            }
            vista.getTxtSaldo().setText("Saldo: " + modelo.getSaldo());
        }
    }

    //abre ventana para agregarse saldo
    private void abrirSaldo() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ControladorSaldo(new VistaSaldo(), modelo).iniciar();
            }
        });
        vista.getTxtSaldo().setText("Saldo:" + modelo.getSaldo());
    }

    //cierra sesion
    private void cerrarSesion() {
        vista.setVisible(false);
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ControladorLogin(new VistaLogin(), new ModeloUsuario()).iniciar();
            }
        });
    }
    
}
