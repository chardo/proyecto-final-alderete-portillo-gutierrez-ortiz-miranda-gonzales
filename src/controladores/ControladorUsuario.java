
package controladores;

import Singleton.Conexion;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import modelos.DAOUsuario;
import modelos.ModeloUsuario;
import vistas.VistaPeliculas;
import vistas.VistaUsuario;

/**
 *
 * @author pepil
 */
public class ControladorUsuario implements ActionListener{
    private VistaUsuario vista;
    private ModeloUsuario modelo;
    private DefaultTableModel tablaRenta;
    private DAOUsuario dao;

    //constructor
    public ControladorUsuario(VistaUsuario vista, ModeloUsuario modelo) {
        this.vista = vista;
        this.modelo = modelo;
        try{
            dao = new DAOUsuario(Conexion.crearConexion());
        }catch(Exception e){
            Logger.getLogger(ControladorUsuario.class.getName()).log(Level.SEVERE, null, e);
        }
        setControl();
    }
    //inicializa vista del usuario
    public void iniciar(){
        vista.setTitle(modelo.getUsuario());
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        llenarTablaPelis();
    }

    //listener para las acciones de la vista
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(vista.getBtnRentar()))
            abrirRenta();
    }

    //asigna control de los botones de la vista
    private void setControl() {
        vista.getBtnRentar().addActionListener(this);
    }
    
    //Inserta las peliculas de la base de datos en la tabla que el usuario que tenga rentadas
    private void llenarTablaPelis(){
        tablaRenta = buildTableModel(dao.consultar("Select idPelicula,fechaInicio,fechaFin from Rentas where idUsuario =" 
                + modelo.getId()));
        vista.getTablaPelis().setModel(tablaRenta);
    }
    public static DefaultTableModel buildTableModel(ResultSet rs) {
        try{
            ResultSetMetaData metaData = rs.getMetaData();
            Vector<String> columnNames = new Vector<String>();
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }

            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
            while (rs.next()) {
                Vector<Object> vector = new Vector<Object>();
                for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                    vector.add(rs.getObject(columnIndex));
                }
                data.add(vector);
            }
            return new DefaultTableModel(data, columnNames);
        }catch(SQLException e){
            Logger.getLogger(ControladorUsuario.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    //abre la vista para rentar peliculas
    private void abrirRenta() {
        vista.setVisible(false);
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ControladorPeliculas(modelo, new VistaPeliculas()).iniciar();
            }
        });
    }
}
