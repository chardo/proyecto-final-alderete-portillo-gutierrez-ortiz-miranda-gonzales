
package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import Singleton.Conexion;
import factory.Factory;
import javax.swing.JOptionPane;
import modelos.DAOUsuario;
import modelos.ModeloUsuario;
import vistas.VistaAgregar;



public class ControladorAgregar implements ActionListener {

    private ModeloUsuario modelo;
    private VistaAgregar vista;
    private DAOUsuario dao;
    
    //Constructor
    public ControladorAgregar(ModeloUsuario modelo, VistaAgregar vista) {
        this.modelo = modelo;
        this.vista = vista;
        
    }
    
    //Inicializa la vista para agregar usuario nuevo
    public void iniciar(){
        vista.setTitle("Agregar");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        try{
            dao = new DAOUsuario(Conexion.crearConexion());
        }catch(Exception e){
            Logger.getLogger(ControladorAgregar.class.getName()).log(Level.SEVERE, null, e);
        }
        
        asignarControl();
    }
    
    private void  asignarControl(){
        vista.getBtnAgregar().addActionListener(this);
    }
    
    //Listener de acción de la vista
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        modelo.setUsuario(vista.getTxtUsu().getText());
        modelo.setContrasena(vista.getTxtPass().getText());

        if(!modelo.getUsuario().equals("") && !modelo.getContrasena().equals("")){
            dao.add(modelo,vista.getTipo().getSelectedItem().toString());
            JOptionPane.showMessageDialog(null, Factory.crearCuenta(vista.getTipo().getSelectedIndex()).Agregado());
        }
        else{

        }
    }
}
