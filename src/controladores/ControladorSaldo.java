
package controladores;

import Singleton.Conexion;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelos.DAOUsuario;
import modelos.ModeloUsuario;
import vistas.VistaSaldo;

public class ControladorSaldo implements ActionListener{
    
    private VistaSaldo vista;
    private ModeloUsuario modelo;
    private float addSaldo = 0.00f;
    private DAOUsuario dao;
    
    //constructor
    public ControladorSaldo(VistaSaldo vista, ModeloUsuario modelo) {
        this.vista = vista;
        this.modelo = modelo;
        setControl();
    }
    //inicializa la vista para agregarse saldo
    public void iniciar(){
        vista.setTitle(modelo.getUsuario());
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }

    //asigna control del boton de la vista
    private void setControl() {
        vista.getBtnAceptar().addActionListener(this);
    }

    //listener de las acciones de la vista
    @Override
    public void actionPerformed(ActionEvent e) {
        try{
            if(!vista.getTxtSaldo().getText().equals("")){
                addSaldo = Float.parseFloat(vista.getTxtSaldo().getText());
                actualizarSaldo();
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "Dato Invalido");
        }
    }

    //agrega a la base de datos el nuevo saldo
    private void actualizarSaldo() {
        modelo.setSaldo(addSaldo+modelo.getSaldo());
        try {
            dao = new DAOUsuario(Conexion.crearConexion());
            dao.actualizarSaldo(modelo);
            JOptionPane.showMessageDialog(null, "Saldo agregado");
        } catch (Exception ex) {
            Logger.getLogger(ControladorSaldo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
