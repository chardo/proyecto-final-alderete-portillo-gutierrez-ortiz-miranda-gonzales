
package modelos;

import Singleton.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

// Clase Genérica
public abstract class DAO<T> {
  
  protected PreparedStatement ps;
  protected ResultSet rs;
  protected Conexion conexion;
  //conexion a la base de datos
  public DAO(Conexion conexion){
    this.conexion = conexion;
  }
  
  public abstract ResultSet consultar(String query);
}
