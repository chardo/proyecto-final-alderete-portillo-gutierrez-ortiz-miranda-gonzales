/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import Singleton.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author pepil
 */
public class DAOUsuario extends DAO<ModeloUsuario>{

    public DAOUsuario(Conexion conexion) {
        super(conexion);
    }
    
    //agrega un usuario con contraseña encriptada
    public void add(ModeloUsuario obj,String tipo){
        try {
          ps = conexion.obtenerConexion().prepareCall("insert into Cliente (usuario,pass,saldo,tipo) values('"+ obj.getUsuario() +"',MD5('" + obj.getContrasena() +"'),0.00,'" + tipo + "')" );      
          ps.execute();
        } catch (SQLException ex) {
          Logger.getLogger(DAOPeliculas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //cambia el saldo del usuario
    public void actualizarSaldo(ModeloUsuario usuario){
        try {
          ps = conexion.obtenerConexion().prepareCall("update Cliente set saldo =" + usuario.getSaldo() + " where id=" + usuario.getId());      
          ps.execute();
        } catch (SQLException ex) {
          Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //consulta en la base de datos
    @Override
    public ResultSet consultar(String query) {
        try {
            ps = conexion.obtenerConexion().prepareCall(query);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
          Logger.getLogger(DAOUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    
}
