
package modelos;

import Singleton.Conexion;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class DAOPeliculas extends DAO<ModeloPeliculas> {

    //conexion a la base de datos
    public DAOPeliculas(Conexion conexion) {
        super(conexion);
    }
    
    //agrega a la base de datos una renta
    public void rentar(int usuario, String pelicula){
        try {
          ps = conexion.obtenerConexion().prepareCall("insert into Rentas (idUsuario,idPelicula,fechaInicio,fechaFin) values("+ usuario +",'" + pelicula +"','" + new Date() + "','" + new Date() +"')" );      
          ps.execute();
          JOptionPane.showMessageDialog(null, "Rentado");
        } catch (SQLException ex) {
          Logger.getLogger(DAOPeliculas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //hace una consulta a la base de datos
    @Override
    public ResultSet consultar(String query) {
        try {
            ps = conexion.obtenerConexion().prepareCall(query);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
          Logger.getLogger(DAOPeliculas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    
}
