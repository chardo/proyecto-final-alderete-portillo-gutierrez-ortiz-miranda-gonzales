
package factory;


public class Factory {
    //metodo de factory que regresa un mensaje segun el usuario creado
    public static Usuario crearCuenta(int tipo){
        
        switch(tipo){
            case 0:
                return new Admin();
            case 1:
                return new Pobre();
            case 2:
                return new VIP();
            default:
                return null;
        }
    }
}
