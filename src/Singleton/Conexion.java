package Singleton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

// Clase con patrón Singleton
public class Conexion {
//Atributo encapsulado de clase del mismo tipo de la clase

  private static Conexion cnn;
  private Connection connection;

  //Constructor privado
  private Conexion() throws ClassNotFoundException, SQLException{
    
    // Localizando a la clase de conexión dentro del paquete
    Class.forName("com.mysql.jdbc.Driver");
            
    // Generamos la conexión con la base de datos
    connection = DriverManager.getConnection(
            "jdbc:mysql://127.0.0.1:3306/ProyectoFinal",
            "root",
            "root"
    ); 
  }

  //Método de clase público con tipo de retorno del mismo tipo de la clase
  public static Conexion crearConexion() throws ClassNotFoundException, SQLException {

    //Control sobre el número de instancias
    if (cnn == null) {
      cnn = new Conexion();
    }
    return cnn;
  }
  
  public Connection obtenerConexion(){
    return connection;
  }

}
